// POP-UP

function imagePopup() {
	$('.test-popup-link').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		zoom: {
			enabled: true,
			duration: 300,
			opener: function(element) {
				return element.find('img');
			}
		}
	});
}



// EMAIL-VALIDATION
 $(function() {
 	$('#myForm').validate({
 		rules: {
 			email: {
 				required: true,
 				email: true
 			}
 		},
 		messages: {
 			email: {
 				required: "You need to type a valid email",
 				email: "This is not a valid email"
 			}
 		}
 	});

 	$('.myform').validate({
 		rules: {
 			email: {
 				required: true,
 				email: true
 			},
 			text: {
 				required: true,
 			},
 			textarea: {
 				required: true,
 			}
 		}
 	});
 });

// CAROUSEL
function carousel() {
	$('.carousel .inner').slick({
		autoplay: false,
		dots: false,
		arrows: true,
		slidesToShow: 1,
		responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		]
	});
}

$(document).ready(function() {
	carousel();
	imagePopup();
});

